import 'package:get_it/get_it.dart';
import 'package:netpeax/core/services/api.dart';
import 'package:netpeax/core/services/fanart_service.dart';
import 'package:netpeax/core/services/local_storage_service.dart';
import 'package:netpeax/core/services/tmdb_service.dart';
import 'package:netpeax/core/services/trakt_service.dart';
import 'package:netpeax/core/services/ui_service.dart';
import 'package:netpeax/core/viewmodels/views/home_view_model.dart';
import 'package:netpeax/core/viewmodels/views/item_view_model.dart';
import 'package:netpeax/core/viewmodels/views/tabs/movies_tab_view_model.dart';
import 'package:netpeax/core/viewmodels/widgets/navbar_model.dart';

GetIt locator = GetIt.instance;

void setupLocator() async {
  final lSService = await LocalStorageService.getInstance();
  locator.registerSingleton<LocalStorageService>(lSService);

  locator.registerLazySingleton(() => FanartService());
  locator.registerLazySingleton(() => TmdbService());
  locator.registerLazySingleton(() => TraktService());
  locator.registerLazySingleton(() => UIService());
  locator.registerLazySingleton(() => Api());

  locator.registerFactory(() => HomeViewModel());
  locator.registerFactory(() => ItemViewModel());
  locator.registerFactory(() => MovieTabViewModel());
  locator.registerFactory(() => NavbarModel());
}

