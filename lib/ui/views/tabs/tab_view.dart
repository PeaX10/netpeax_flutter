import 'package:flutter/material.dart';

abstract class TabView extends StatelessWidget {
  final String title = "";

  String getTitle() {
    return title;
  }
}