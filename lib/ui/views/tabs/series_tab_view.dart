import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/ui/shared/text_styles.dart';
import 'package:netpeax/ui/views/tabs/tab_view.dart';

class SeriesTabView extends TabView {

  final String title = "series";

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Les deniers series", style: headerStyle,)
          ]
      ),
    );
  }

}