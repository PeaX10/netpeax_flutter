import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/viewmodels/views/tabs/movies_tab_view_model.dart';
import 'package:netpeax/ui/base_widget.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:netpeax/ui/views/item_view.dart';
import 'package:netpeax/ui/views/tabs/tab_view.dart';
import 'package:netpeax/ui/widgets/list_item.dart';
import 'package:netpeax/ui/widgets/nav_section.dart';
import 'package:netpeax/ui/widgets/preview_item.dart';

class MoviesTabView extends TabView {
  final String title = "movies";

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MovieTabViewModel>(
        model: MovieTabViewModel(),
        onModelReady: (MovieTabViewModel model) async {
          model.setContext(context);
          await model.getMovies();
        },
        builder: (context, model, child) => !model.busy &&
                model.sections != null
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                    gradient: new LinearGradient(
                        begin: const Alignment(0, 0),
                        end: const Alignment(0, 1),
                        colors: <Color>[
                      Colors.black.withOpacity(0.5),
                      Colors.black.withOpacity(0.4),
                      Colors.black.withOpacity(0.3),
                      Colors.black.withOpacity(0),
                    ])),
                child: Column(
                  children: [
                    Row(
                      children: [
                        PreviewItem(
                          item: model.sections[model.section][model.index],
                        ),
                        NavSection(
                          sections: model.sections.keys.toList(),
                          seleted: model.section,
                          onTap: model.onTapSection(model.section),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        !model.busy && model.sections[model.section] != null
                            ? ListItem(
                                items: model.sections[model.section],
                                scrollController: model.scrollController,
                                focusNodes: model.focusNodes,
                                onReady: (int i) async {
                                  await model.loadTranslations(i);
                                  await model.loadImage(i);
                                },
                                onFocusChange: model.onFocusChangeUpdateSection,
                                onFocus: (int i) {
                                  model.updateIndex(i);
                                },
                              )
                            : CircularProgressIndicator(),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            minWidth: UIHelper.getListItemSize(context).width,
                            maxWidth: UIHelper.getListItemSize(context).width,
                            maxHeight: 5,
                          ),
                          child: RaisedButton(
                            color: Colors.transparent,
                            onPressed: () {},
                            focusNode: model.bottomFocusNode,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(
                    Colors.white.withOpacity(0.5)),
              )));
  }
}
