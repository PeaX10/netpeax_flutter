import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/core/viewmodels/views/home_view_model.dart';
import 'package:netpeax/core/viewmodels/views/item_view_model.dart';
import 'package:netpeax/ui/base_widget.dart';
import 'package:netpeax/ui/shared/text_styles.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:netpeax/ui/views/tabs/movies_tab_view.dart';
import 'package:netpeax/ui/views/tabs/series_tab_view.dart';
import 'package:netpeax/ui/views/tabs/tab_view.dart';
import 'package:netpeax/ui/widgets/navbar.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class ItemView extends StatelessWidget {

  static const String route = "item";
  final Item item;

  ItemView({this.item});


  @override
  Widget build(BuildContext context) {
    return BaseWidget<ItemViewModel>(
        model: ItemViewModel(),
        onModelReady: (ItemViewModel model) {
          model.setContext(context);
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: Provider.of<UI>(context).backgroundUrl ?? "",
                imageBuilder: (context, imageProvider) {
                  return Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                          //colorFilter: ColorFilter.mode(Colors.black, BlendMode.colorDodge)
                      ),
                    ),
                  );
                },
                fadeInCurve: Curves.easeIn,
                fadeOutCurve: Curves.easeInCirc,
                placeholderFadeInDuration: Duration(seconds: 2),
                placeholder: (context, url) => Image.memory(kTransparentImage, fit: BoxFit.fill),
                fadeOutDuration: Duration(seconds: 2),
                fadeInDuration: Duration(seconds: 2),
              ),
              Center(
                child: Text('COUCOU', style: headerStyle,)
              )
            ],
          ),
        )
    );
  }

}