import 'package:flutter/material.dart';
import 'package:netpeax/ui/shared/text_styles.dart';
import 'package:netpeax/core/viewmodels/widgets/navbar_model.dart';
import 'package:netpeax/ui/base_widget.dart';

import 'package:netpeax/ui/views/tabs/tab_view.dart';

class NavBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize; // default is 56.0

  final TabController tabController;
  final List<TabView> tabs;

  NavBar({List<TabView> tabs, TabController tabController, Key key}) : tabs = tabs, tabController = tabController, preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);


  @override
  _NavBarState createState() => _NavBarState(tabController, tabs);
}

class _NavBarState extends State<NavBar>{
  TabController tabController;
  List<TabView> tabs;

  _NavBarState(this.tabController, this.tabs) {
    this.tabController = tabController;
    this.tabs = tabs;
  }


  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavbarModel>(
        model: NavbarModel(),
        onModelReady: (NavbarModel model) => model.setContext(context),
        builder: (context, model, child) => AppBar(
          backgroundColor: Colors.black.withOpacity(0.5),
          titleSpacing: 0,
          leading: model.busy ? const BackButton() : Container(width: 0,),
          title: model.busy ? _buildSearchField(model) : _buildMenu(model),
          actions: _buildActions(model, context),
        )
    );
  }

  Widget _buildMenu(NavbarModel model) {
    return TabBar(
      isScrollable: true,
      indicatorColor: Colors.white,
      controller: tabController,
      tabs: tabs.map((TabView tab) => Tab(child: Text(tab.getTitle(), style: model.isActive(tab.getTitle()) ? navbarActiveStyle : navbarStyle)),).toList(),
    );
  }

  Widget _buildSearchField(NavbarModel model) {
    return TextField(
      controller: model.searchQueryController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: "Search somethings...",
        border: InputBorder.none,
        hintStyle: navbarStyle,
      ),
      style: TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: (query) => model.updateSearchQuery,
    );
  }

  List<Widget> _buildActions(NavbarModel model, BuildContext context) {
    if (model.busy) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            if (model.searchQueryController == null ||
                model.searchQueryController.text.isEmpty) {
              Navigator.pop(context);
            }
            model.clearSearchQuery();
          },
        ),
      ];
    }

    return <Widget>[
      IconButton(
        icon: const Icon(Icons.search),
        onPressed: model.startSearch,
      ),
      IconButton(
        icon: const Icon(Icons.settings),
        onPressed: model.settingsPage,
      ),
    ];
  }
}