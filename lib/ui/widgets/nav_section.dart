import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/ui/shared/text_styles.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class NavSection extends StatelessWidget {

  List<String> sections;
  String seleted;
  Function onTap;

  NavSection({this.sections, this.seleted, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: UIHelper.getSectionsNavSize(context).height,
      width: UIHelper.getSectionsNavSize(context).width,
      margin: EdgeInsets.only(top: UIHelper.getNavbarHeight(context)),
      padding: EdgeInsets.only(right: 20),
      child: sections != null ?
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: sections.map((section) => InkWell(
              onTap: onTap,
              child: Text(section, style: seleted == section ? sectionActiveStyle : sectionStyle,)),
            ).toList()
          )
          : null,
    );
  }
}
