import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:netpeax/ui/views/item_view.dart';
import 'package:netpeax/ui/widgets/cover.dart';

class ListItem extends StatelessWidget {
  final List<Item> items;
  final List<FocusNode> focusNodes;
  final ScrollController scrollController;
  final Function onReady;
  final Function onTap;
  final Function onFocus;
  final Function onFocusChange;
  ListItem({this.items, this.scrollController, this.onReady, this.onFocus, this.onFocusChange, this.onTap, this.focusNodes});

  @override
  Widget build(BuildContext context) {
    return _buildListItem(context); // TODO : Animate List
  }

  Widget _buildListItem(BuildContext context) {
    return Container(
        height: UIHelper.getListItemSize(context).height - 5,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: ListView.builder(
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: items.length,
          itemBuilder: (BuildContext context, int i) {
            return items.asMap().containsKey(i)
                ? Cover(
                    item: items[i],
                    index: i,
                    focusNode: focusNodes[i],
                    onFocusChange: onFocusChange,
                    onFocus: () => onFocus(i),
                    onReady: () async => await onReady(i),
                    onTap: () => Navigator.of(context).pushNamed(ItemView.route, arguments: items[i]),
                ) : Container();
          },
        ));
  }
}
