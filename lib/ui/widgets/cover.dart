import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:provider/provider.dart';

class Cover extends StatefulWidget {
  const Cover({
    Key key,
    @required this.item,
    @required this.onTap,
    @required this.index,
    @required this.focusNode,
    @required this.onReady,
    @required this.onFocus,
    @required this.onFocusChange,
  }) : super(key: key);

  final Item item;
  final Function onTap;
  final int index;
  final FocusNode focusNode;
  final Function onReady;
  final Function onFocus;
  final Function onFocusChange;


  @override
  _CoverState createState() => _CoverState();
}

class _CoverState extends State<Cover> with SingleTickerProviderStateMixin  {

  AnimationController _controller;
  Animation<double> _animation;
  int _focusAlpha = 100;

  Widget image;

  @override
  void initState() {
    widget.onReady();
    widget.focusNode.addListener(_onFocusChange);
    _controller = AnimationController(duration: const Duration(milliseconds: 100), vsync: this, lowerBound: 0.9, upperBound: 1);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    widget.focusNode.dispose();
    super.dispose();
  }

  void _onFocusChange() async {
      if(widget.focusNode.hasFocus) {
        _controller.forward();
        if(widget.onFocus != null) {
          await widget.onFocus();
        }
    } else {
        widget.onFocusChange();
        _controller.reverse();
      }
  }

  void _onTap() {
    widget.focusNode.requestFocus();
    if(widget.onTap != null) {
      widget.onTap();
    }
  }


  @override
  Widget build(BuildContext context) {
      return RawMaterialButton(
        onPressed: _onTap,
        focusNode: widget.focusNode,
        focusColor: Colors.transparent,
        focusElevation: 0,
        child: buildCover(context),
      );
    }

  

  Widget buildCover(BuildContext context) {
    return  ScaleTransition(
      scale: _animation, 
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: _onTap,
        child: SizedBox(
          width: UIHelper.getCoverSize(context).width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget> [
              Container(
                child: buildPosterImage(context),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withAlpha(_focusAlpha),
                        blurRadius: 15,
                        offset: Offset(10, 15),
                      )
                    ]
                ),
              ),
              SizedBox(
                height: 20,
                child: Center(
                  child: Align(child: Text(widget.item.title, maxLines: 1, style: TextStyle(color: Colors.white),overflow: TextOverflow.ellipsis,), alignment: Alignment.topLeft,),
                ),
              )
            ],
          ),
        )
      ),
    );
  }

  Widget buildPosterImage(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: widget.item.tmdbResponse?.poster ?? "",
      placeholder: (context, url) => buildPlaceholder(context),
      errorWidget: (context, url, error) => buildPlaceholder(context),
      placeholderFadeInDuration: Duration(seconds: 2),
      fadeOutDuration: Duration(seconds: 2),
      fadeInDuration: Duration(seconds: 2),
    );
  }
  
  Widget buildPlaceholder(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: UIHelper.getPosterSize(context).height,
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.4)
          ),
        ),
        new BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 1, sigmaY: 1),
          child: new Container(
            decoration: new BoxDecoration(
                color: Colors.white.withOpacity(0.5)),
          ),
        )
      ],
    );
  }
}


  