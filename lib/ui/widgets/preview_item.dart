import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/ui/shared/text_styles.dart';
import 'package:netpeax/ui/shared/ui_helpers.dart';
import 'package:transparent_image/transparent_image.dart';

class PreviewItem extends StatelessWidget {
  final Item item;

  PreviewItem({this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: UIHelper.getNavbarHeight(context)),
      child: item != null
          ? Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
              width: UIHelper.getPreviewItemSize(context).width,
              height: UIHelper.getPreviewItemSize(context).height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  item.fanartResponse?.hdLogos != null
                      ? Container(
                          height: 75,
                          child: CachedNetworkImage(
                            imageUrl: item?.fanartResponse?.hdLogos[0].url,
                            fadeInCurve: Curves.easeIn,
                            fadeOutCurve: Curves.easeInCirc,
                            placeholderFadeInDuration: Duration(seconds: 1),
                            placeholder: (context, url) => Image.memory(
                                kTransparentImage,
                                fit: BoxFit.fill),
                            fadeOutDuration: Duration(seconds: 1),
                            fadeInDuration: Duration(seconds: 1),
                          ),
                        )
                      : item.fanartResponse?.logos != null
                          ? Container(
                              height: 75,
                              child: CachedNetworkImage(
                                imageUrl: item?.fanartResponse?.logos[0].url,
                                fadeInCurve: Curves.easeIn,
                                fadeOutCurve: Curves.easeInCirc,
                                placeholderFadeInDuration: Duration(seconds: 1),
                                placeholder: (context, url) => Image.memory(
                                    kTransparentImage,
                                    fit: BoxFit.fill),
                                fadeOutDuration: Duration(seconds: 1),
                                fadeInDuration: Duration(seconds: 1),
                              ),
                            )
                          : Text(
                              "${item.title} (${item.year})",
                              style: titlePreview,
                              overflow: TextOverflow.ellipsis,
                            ),
                  UIHelper.verticalSpaceSmall,
                  Row(
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.white.withOpacity(0.6),
                      ),
                      Text(item.rating.toStringAsPrecision(3),
                          style: infosPreview),
                      UIHelper.horizontalSpaceSmall,
                      Icon(
                        Icons.timer,
                        color: Colors.white.withOpacity(0.6),
                      ),
                      Text(item.durationInHours(), style: infosPreview)
                    ],
                  ),
                  UIHelper.verticalSpaceMedium,
                  Expanded(
                    child: Text(
                      item.overview,
                      style: overviewPreview,
                    ),
                  )
                ],
              ),
            )
          : null,
    );
  }
}
