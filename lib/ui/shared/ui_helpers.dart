import 'package:flutter/material.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:provider/provider.dart';

/// Contains useful consts to reduce boilerplate and duplicate code
class UIHelper {
  static const double _VerticalSpaceSmall = 10.0;
  static const double _VerticalSpaceMedium = 20.0;
  static const double _VerticalSpaceLarge = 60.0;

  static const double _HorizontalSpaceSmall = 10.0;
  static const double _HorizontalSpaceMedium = 20.0;
  static const double _HorizontalSpaceLarge = 60.0;
  
  static const double _PosterRatio = 2/3;

  static const Widget verticalSpaceSmall = SizedBox(height: _VerticalSpaceSmall);
  static const Widget verticalSpaceMedium = SizedBox(height: _VerticalSpaceMedium);
  static const Widget verticalSpaceLarge = SizedBox(height: _VerticalSpaceLarge);

  static const Widget horizontalSpaceSmall = SizedBox(width: _HorizontalSpaceSmall);
  static const Widget horizontalSpaceMedium = SizedBox(width: _HorizontalSpaceMedium);
  static const Widget horizontalSpaceLarge = SizedBox(width: _HorizontalSpaceLarge);

  static double getNbItems(BuildContext context) {
      return (getListItemSize(context).width / (getListItemSize(context).height - 20 - 20*2)*(1/_PosterRatio)); // 20*2: padding
  }

  static Size getListItemSize(BuildContext context){
      UI ui = Provider.of<UI>(context);
      double width = ui.screenResolution.width;
      double height = ui.screenResolution.height*0.5 - getNavbarHeight(context);
      
      return new Size(width, height);
  }

  static Size getPreviewItemSize(BuildContext context){
    UI ui = Provider.of<UI>(context);
    double width = ui.screenResolution.width * 0.6;
    double height = ui.screenResolution.height - getListItemSize(context).height - getNavbarHeight(context);
    return new Size(width, height);
  }

  static Size getSectionsNavSize(BuildContext context){
    UI ui = Provider.of<UI>(context);
    double width = ui.screenResolution.width * 0.4;
    double height = ui.screenResolution.height - getListItemSize(context).height - getNavbarHeight(context);
    return new Size(width, height);
  }

  static Size getCoverSize(BuildContext context){
      UI ui = Provider.of<UI>(context);
      double widthPadding = 0;

      double width = (ui.screenResolution.width - widthPadding) / getNbItems(context);
      double height = getListItemSize(context).height;

      return new Size(width, height);
  }

  static Size getPosterSize(BuildContext context) {
      UI ui = Provider.of<UI>(context);
      double posterRatio = 2/3;
      Size coverSize = getCoverSize(context);
      return new Size(coverSize.width, coverSize.width * (1/posterRatio));
  }

  static double getNavbarHeight(BuildContext context) {
      UI ui = Provider.of<UI>(context);
      if(ui.screenResolution.height < 200) {
          return 30;
      } else if(ui.screenResolution.height <= 540) {
        return 40;
      } else if(ui.screenResolution.height <= 1080) {
        return 60;
      } else {
        return 80;
      }
  }
}