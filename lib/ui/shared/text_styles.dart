import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const List<Shadow> blackShadows = [
  Shadow(
  offset: Offset(1.0, 1.0),
  blurRadius: 3.0,
  color: Colors.black,
  ),
];

const List<Shadow> whiteShadows = [
  Shadow(
    offset: Offset(0.0, 0.0),
    blurRadius: 1.0,
    color: Colors.white,
  ),
];

const TextStyle h1 = TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 30);
const TextStyle h2 = TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 26);
const TextStyle h3 = TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 22);
const TextStyle h4 = TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 16);
const TextStyle h5 = TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 12);
const TextStyle h6 = TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 8);

TextStyle headerStyle = GoogleFonts.lato(textStyle: h3, color: Colors.white);
TextStyle navbarStyle = GoogleFonts.lato(textStyle: h5, color: Colors.white);
TextStyle navbarActiveStyle = GoogleFonts.lato(textStyle: h5, color: Colors.white);

TextStyle sectionActiveStyle = GoogleFonts.lato(textStyle: h1, color: Colors.white);
TextStyle sectionStyle = GoogleFonts.lato(textStyle: h3, color: Colors.white);

TextStyle titlePreview = GoogleFonts.lato(textStyle: h1, color: Colors.white, shadows: blackShadows);
TextStyle overviewPreview = GoogleFonts.lato(textStyle: h5, color: Colors.white.withOpacity(0.8));
TextStyle infosPreview = GoogleFonts.lato(textStyle: h4, color: Colors.white.withOpacity(0.6), shadows: blackShadows);
