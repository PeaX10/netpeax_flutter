import 'package:netpeax/core/enums/language.dart';

class FanartItem {
  int id;
  String url;
  Language lang;
  String liked;
  int season;

  FanartItem({this.id, this.url, this.lang, this.liked, this.season});

  factory FanartItem.fromJson(Map<String, dynamic> json) {
    FanartItem fanartItem = new FanartItem();
    fanartItem.id = json.containsKey('id') ? int.parse(json['id']) : null;
    fanartItem.url = json['url'] ?? null;
    //fanartItem.lang = Language.values.firstWhere((e) => e.toShortString() == json['lang']) ?? Language.EN;
    fanartItem.liked = json['url'] ?? null;
    fanartItem.season = json['season'] as int;

    return fanartItem;
  }
}