class TmdbResponse {
  String poster;
  String backDrop;
  String still;
  TmdbResponse({this.poster, this.backDrop, this.still});
}