import 'package:netpeax/core/models/api_responses/fanart_item.dart';

class FanartResponse {
  List<FanartItem> hdClearLogos;
  List<FanartItem> hdClearArts;
  List<FanartItem> hdLogos;
  List<FanartItem> posters;
  List<FanartItem> backgrounds;
  List<FanartItem> discs;
  List<FanartItem> arts;
  List<FanartItem> banners;
  List<FanartItem> logos;
}