import 'package:flutter/cupertino.dart';
import 'package:netpeax/core/enums/genre.dart';
import 'package:netpeax/core/enums/type.dart';
import 'package:netpeax/core/models/api_responses/fanart_response.dart';
import 'package:netpeax/core/models/api_responses/tmdb_response.dart';
import 'package:intl/intl.dart';
import 'package:netpeax/core/models/translation.dart';

class Item {

  String title;
  Type type;
  String overview;
  String tagline;
  int runtime;
  int number;
  int seasonNumber;
  String network;
  String trailer;
  double rating;
  String firstAired;
  List<Genre> genres;
  int year;
  List <Translation> translations;
  Map<String, dynamic> ids;
  Map<String, dynamic> showIds;
  TmdbResponse tmdbResponse;
  FanartResponse fanartResponse;
  List<Item> seasons;
  List<Item> episodes;

  Item({
    this.seasons,
    this.episodes,
    this.title,
    this.type,
    this.year,
    this.overview,
    this.tagline,
    this.trailer,
    this.number,
    this.seasonNumber,
    this.runtime,
    this.firstAired,
    this.rating,
    this.ids,
    this.showIds,
  });

  bool isMovie() {
    return this.type == Type.MOVIE;
  }

  bool isShow() {
    return this.type == Type.SHOW;
  }

  bool isSeason() {
    return this.type == Type.SEASON;
  }

  bool isEpisode() {
    return this.type == Type.EPISODE;
  }

  factory Item.fromJson(Map<String, dynamic> json, Type type, {Item show}) {
    Item item = Item();

    if(type == Type.MOVIE || type == Type.SHOW) {
      String t = type.toShortString();
      if(json.containsKey(t)) json = json[t];
      item.title = json['title'] as String;
      item.type = type;
      item.year = json['year'] as int;
      item.overview = json['overview'] as String;
      item.runtime = json['runtime'] as int;
      item.trailer = json['trailer'] as String;
      item.rating = json['rating'] as double;
      item.ids = json['ids'] as Map<String, dynamic>;

      item.genres = new List<Genre>();
      List<dynamic> genres = json['genres'];
      item.genres = genres.map((genre) => Genre.values.firstWhere((element) => element.getSlug() == genre.toString())).toList();
    }

    if(type == Type.SEASON) {
      item.title = json['title'] as String;
      item.type = type;
      item.showIds = show.ids;
      item.number = json['number'] as int;
      if(json['first_aired'] != null) {
        item.year = DateTime.parse(json['first_aired']).year;
      } else {
        item.year = show.year;
      }

      item.overview = json['overview'] as String;
      item.runtime = json['runtime'] as int;
      item.trailer = json['trailer'] as String;
      item.rating = json['rating'] as double;
      item.ids = json['ids'] as Map<String, dynamic>;

      item.episodes = List<Item>();

      for(int i = 0; i < json['episodes'].length; i++) {
        Item episode = Item();
        episode.title = json['episodes'][i]['title'] as String;
        episode.type = Type.EPISODE;
        episode.number = json['episodes'][i]['number'] as int;
        episode.seasonNumber = item.number;
        episode.showIds = show.ids;
        if(json['episodes'][i]['first_aired'] != null) {
          episode.year = DateTime.parse(json['episodes'][i]['first_aired']).year;
          episode.firstAired = new DateFormat("d.M.y").format(DateTime.parse(json['episodes'][i]['first_aired'])).toString();
          item.year = episode.year;
        } else {
          episode.year = item.year;
          episode.firstAired = '';
        }
        episode.overview = (json['episodes'][i]['overview'] != null ? json['episodes'][i]['overview'] : '') as String;

        episode.runtime = json['episodes'][i]['runtime'] as int;
        episode.trailer = json['episodes'][i]['trailer'] as String;
        episode.rating = json['episodes'][i]['rating'] as double;
        episode.ids = json['episodes'][i]['ids'] as Map<String, dynamic>;

        item.episodes.add(episode);

      }

    }
    return item;

  }

  String durationInHours() {
    var d = Duration(minutes:this.runtime);
    List<String> parts = d.toString().split(':');
    return '${parts[0].padLeft(2, '0')}h ${parts[1].padLeft(2, '0')}';
  }

}