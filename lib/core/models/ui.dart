import 'package:flutter/cupertino.dart';
import 'package:netpeax/core/models/item.dart';

class UI {
  double screenRatio;
  Size screenResolution;
  String backgroundUrl;
  UI({this.screenRatio, this.screenResolution ,this.backgroundUrl});

  UI.initial()
      : backgroundUrl = null;
}