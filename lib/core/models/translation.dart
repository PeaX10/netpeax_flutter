class Translation {

  String title;
  String overview;
  String tagline;
  String language;

  Translation({
    this.title,
    this.overview,
    this.tagline,
    this.language,
  });

  factory Translation.fromJson(Map<String, dynamic> json) {
    Translation translation = new Translation();
    translation.title = json['title'];
    translation.overview = json['overview'];
    translation.tagline = json['tagline'];
    //translation.language = Language.values.firstWhere((e) => e.toShortString() == json['language']);
    translation.language = json['language'];

    return translation;

  }

}