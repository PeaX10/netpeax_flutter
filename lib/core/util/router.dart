import 'package:flutter/material.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/ui/views/home_view.dart';
import 'package:netpeax/ui/views/item_view.dart';

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeView.route:
        return MaterialPageRoute(builder: (_) => HomeView());
      case ItemView.route:
        Item item = settings.arguments as Item;
        return MaterialPageRoute(builder: (_) => ItemView(item: item,));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}