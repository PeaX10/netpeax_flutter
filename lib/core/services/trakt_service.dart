import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:netpeax/core/enums/genre.dart';
import 'package:netpeax/core/enums/movie_endpoint.dart';
import 'package:netpeax/core/enums/type.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/translation.dart';
import 'package:netpeax/core/services/tmdb_service.dart';
import 'package:netpeax/locator.dart';

import 'fanart_service.dart';

class TraktService {
  static const String apiVersion = '2';
  static const String baseUrl = 'https://api.trakt.tv';
  static const String apiKey = "d3966fc8ff7a20b517e6bc5d60afd5ef145775c1f6d74328d6706993927ee615";

  Map<String, String> headers = {'trakt-api-version': apiVersion, 'trakt-api-key': apiKey};

  Future<List<Item>> getMovies(int limit, MovieEndpoint endpoint) async {
    var res = await http.get('$baseUrl/movies/${endpoint.toShortString()}?extended=full&limit=${limit.toString()}', headers: {'trakt-api-version': apiVersion, 'trakt-api-key': apiKey});
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Item> movies = body
          .map(
            (dynamic item) => Item.fromJson(item, Type.MOVIE),
          )
          .toList();
      return movies;
    } else {
      print(res.statusCode);
      print(res.body);
      return List<Item>();
    }
  }

  Future<List<Translation>> getTranslations(Item item) async {
    var res = await http.get('$baseUrl/${item.type.toShortString()}s/${item.ids['trakt']}/translations', headers: headers);
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Translation> translations = body
          .map(
            (dynamic translation) => Translation.fromJson(translation),
          )
          .toList();
      return translations;
    } else {
      print(res.statusCode);
      print(res.body);
      return List<Translation>();
    }
  }


  Future<List<Item>> getShows(int limit) async {
    var res = await http.get('${baseUrl}/shows/trending?extended=full&limit=${limit.toString()}', headers: {'trakt-api-version': apiVersion, 'trakt-api-key': apiKey});
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Item> shows = body
          .map(
            (dynamic item) => Item.fromJson(item, Type.SHOW),
          )
          .toList();
      return shows;
    } else {
      print(res.statusCode);
      print(res.body);
      return List<Item>();
      // throw "Can't get posts.";
    }
  }

  Future<List<Item>> getSeasons(Item show) async {
    var res = await http.get('${baseUrl}/shows/${show.ids['trakt']}/seasons?extended=episodes,full', headers: {'trakt-api-version': apiVersion, 'trakt-api-key': apiKey});
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Item> seasons = body
          .map(
            (dynamic item) => Item.fromJson(item, Type.SEASON, show: show),
          )
          .toList();
      return seasons;
    } else {
      print(res.statusCode);
      print(res.body);
      return List<Item>();
    }
  }

}