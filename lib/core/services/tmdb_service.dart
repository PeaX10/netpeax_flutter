import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:netpeax/core/enums/type.dart';
import 'package:netpeax/core/models/api_responses/tmdb_response.dart';
import 'package:netpeax/core/models/item.dart';

class TmdbService {
  String tmdbApiUrl = 'https://api.themoviedb.org/3';
  String tmdbApiKey = '7f278e56a85aa1aa3ead3db5cfffbc31';
  String tmdbImageUrl = 'https://image.tmdb.org/t/p/w500';

  Future<TmdbResponse> getImages(Item item) async {
    var res = await http.get('${tmdbApiUrl}/${_getTmdbEndpoint(item)}?api_key=${tmdbApiKey}');
    TmdbResponse response = new TmdbResponse();
    if (res.statusCode == 200) {
      Map<String, dynamic> body = jsonDecode(res.body);

      if(body['still_path'] != null) {
        response.still = tmdbImageUrl + body['still_path'];
      }
      if(body['poster_path'] != null) {
        response.poster = tmdbImageUrl + body['poster_path'];
      }
      if(body['backdrop_path'] != null) {
        response.backDrop = (tmdbImageUrl + body['backdrop_path']).replaceAll('w500', 'original');
      }
    }
    return response;
  }

  String _getTmdbEndpoint(Item item) {
    switch(item.type) {
      case Type.MOVIE:
        return 'movie/${item.ids['tmdb'].toString()}';
      case Type.SHOW:
        return 'tv/${item.ids['tmdb'].toString()}';
      case Type.SEASON:
        return 'tv/${item.showIds['tmdb'].toString()}/season/${item.number}';
      case Type.EPISODE:
        return 'tv/${item.showIds['tmdb'].toString()}/season/${item.seasonNumber}/episode/${item.number}';
      case Type.ANIME:
        // TODO: Handle this case.
        break;
    }
    return '';
  }

  

}

