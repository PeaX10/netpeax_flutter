import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:netpeax/core/enums/type.dart';
import 'package:netpeax/core/models/api_responses/fanart_item.dart';
import 'package:netpeax/core/models/api_responses/fanart_response.dart';
import 'package:netpeax/core/models/item.dart';

class FanartService {
  String baseUrl = 'http://webservice.fanart.tv/v3';
  String apiKey = '2b8c357c5e8b9fa796d97fcf4721bd68';


  Future<FanartResponse> getImages(Item item) async {
    String endpoint = _getEndpoint(item);
    var res = await http.get('${baseUrl}/${endpoint}?api_key=${apiKey}');
    if (res.statusCode == 200) {
      Map<String, dynamic> body = jsonDecode(res.body);
      switch (item.type) {
        case Type.MOVIE:
          return _fetchMovieData(item, body);
        case Type.SHOW:
          // TODO: Handle this case.
          return null;
          break;
        case Type.ANIME:
          // TODO: Handle this case.
          return null;
          break;
        case Type.SEASON:
          // TODO: Handle this case.
          return null;
          break;
        case Type.EPISODE:
          // TODO: Handle this case.
          return null;
          break;
      }
    } else {
      return null;
    }
  }

  FanartResponse _fetchMovieData(Item item, Map<String, dynamic> body){
      FanartResponse fanartResponse = new FanartResponse();
      if(body.containsKey('moviebackground')) fanartResponse.backgrounds = _getFanartItem(body['moviebackground']);
      if(body.containsKey('moviedisc')) fanartResponse.discs = _getFanartItem(body['moviedisc']);
      if(body.containsKey('moviebanner')) fanartResponse.banners = _getFanartItem(body['moviebanner']);
      if(body.containsKey('movieposter')) fanartResponse.posters = _getFanartItem(body['movieposter']);
      if(body.containsKey('moviethumb')) fanartResponse.discs = _getFanartItem(body['moviethumb']);
      if(body.containsKey('hdmovielogo')) fanartResponse.hdLogos = _getFanartItem(body['hdmovielogo']);
      if(body.containsKey('movielogo')) fanartResponse.logos = _getFanartItem(body['movielogo']);
      if(body.containsKey('hdmovieclearart')) fanartResponse.hdClearArts = _getFanartItem(body['hdmovieclearart']);
      if(body.containsKey('movieart')) fanartResponse.arts = _getFanartItem(body['movieart']);

      return fanartResponse;
  }

  List<FanartItem> _getFanartItem(List body){
    List<FanartItem> fanartItems = new List<FanartItem>();
    body.forEach((element) {
      fanartItems.add(FanartItem.fromJson(element));
    });
    return fanartItems;
  }

  String _getEndpoint(Item item) {
    if(item.type == null) return null;

    if(item.type == Type.MOVIE)
      return 'movies/${item.ids['imdb'].toString()}';
    return 'tv/${item.ids['imdb'].toString()}';
  }

}

