enum MovieEndpoint {
  TRENDING,
  POPULAR,
  PLAYED,
  WATCHED,
  COLLECTED,
  ANTICIPATED,
}

extension ParseToString on MovieEndpoint {
  String toShortString() {
    return this.toString().split('.').last.toLowerCase();
  }
}