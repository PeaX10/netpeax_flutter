enum Type {
  SHOW,
  MOVIE,
  ANIME,
  SEASON,
  EPISODE
}

extension ParseToString on Type {
  String toShortString() {
    return this.toString().split('.').last.toLowerCase();
  }
}