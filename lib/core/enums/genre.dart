enum Genre {
  ACTION,
  ADVENTURE,
  ANIMATION,
  ANIME,
  COMEDY,
  CRIME,
  DISASTER,
  DOCUMENTARY,
  DRAMA,
  FAMILY,
  FAN_FILM,
  FANTASY,
  FILM_NOIR,
  HISTORY,
  HOLIDAY,
  HORROR,
  INDIE,
  MUSIC,
  MUSICAL,
  MYSTERY,
  NONE,
  ROAD,
  ROMANCE,
  SCIENCE_FICTION,
  SHORT,
  SPORTS,
  SPORTING_EVENT,
  SUPERHERO,
  SUSPENSE,
  THRILLER,
  TV_MOVIE,
  WAR,
  WESTERN,
}

extension ParseToString on Genre {
  String getName() {
    return this.toString().split('.').last.toLowerCase().split('_').map((e) => "${e[0].toUpperCase()}${e.substring(1)}").join(' ');
  }

  String getSlug() {
    return this.toString().split('.').last.toLowerCase().replaceAll("_", "-");
  }
}