import 'package:flutter/material.dart';
import 'package:netpeax/locator.dart';

class BaseModel extends ChangeNotifier {

  bool _busy = false;
  bool get busy => _busy;

  BuildContext context;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }

  setContext(BuildContext ctx) => context = ctx;
}