
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:netpeax/core/enums/language.dart';
import 'package:netpeax/core/enums/movie_endpoint.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/translation.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/core/services/fanart_service.dart';
import 'package:netpeax/core/services/tmdb_service.dart';
import 'package:netpeax/core/services/trakt_service.dart';
import 'package:netpeax/core/services/ui_service.dart';
import 'package:netpeax/core/viewmodels/base_model.dart';
import 'package:netpeax/locator.dart';
import 'package:provider/provider.dart';

class MovieTabViewModel extends BaseModel {

  static const _nbItems = 16;
  TraktService _traktService = locator<TraktService>();
  TmdbService _tmdbService = locator<TmdbService>();
  FanartService _fanartService = locator<FanartService>();
  UIService _uiService = locator<UIService>();


  Map<String, List<Item>> _sections = new Map<String, List<Item>>();
  Map<String, List<Item>> get sections => _sections;

  List<FocusNode> _focusNodes = new List<FocusNode>();
  List<FocusNode> get focusNodes => _focusNodes;

  String _section;
  String get section => _section;

  int _index;
  int get index => _index;

  _initFocusNodes() {
    for(int k = 0; k < _nbItems; k++){
      _focusNodes.add(new FocusNode());
    }
  }

  Future getMovies() async {
    setBusy(true);
    _initFocusNodes();
    _sections["trending"] = await _traktService.getMovies(_nbItems, MovieEndpoint.TRENDING);
    _sections["watched"] = await _traktService.getMovies(_nbItems, MovieEndpoint.WATCHED);
    _sections["popular"] = await _traktService.getMovies(_nbItems, MovieEndpoint.POPULAR);
    _section = _sections.keys.first;
    _index = 0;
    setBusy(false);
  }

  ScrollController _scrollController = new ScrollController();
  ScrollController get scrollController => _scrollController;

  FocusNode get bottomFocusNode => _bottomFocusNode;
  FocusNode _bottomFocusNode = FocusNode();

  Future loadImage(int i) async {
    _sections[_section][i].tmdbResponse = await _tmdbService.getImages(_sections[_section][i]);
    _sections[_section][i].fanartResponse = await _fanartService.getImages(_sections[_section][i]);

    if(i == 0) {
      if (_sections[_section][_index].tmdbResponse ==
          null) { // _index if user focus _index != 0 during _index = 0 is loading
        await loadImage(_index);
      }
      updateIndex(_index);
      _focusNodes[_index].requestFocus();
    }
    notifyListeners();
  }

  Future loadTranslations(int i) async {
    if (_sections[_section][i].translations == null || _sections[_section][i].translations.length == 0) {
      _sections[_section][i].translations = await _traktService.getTranslations(_sections[_section][i]);
      _sections[_section][i].translations.forEach((Translation translation) {
        if(translation.language == Language.FR.toShortString()){
          _sections[_section][i].overview = translation.overview;
          _sections[_section][i].title = translation.title;
          _sections[_section][i].tagline = translation.tagline;
        }
      });
    }
    notifyListeners();
  }


  updateIndex(int i) {
    setBusy(true);
    _index = i;
    updateBackDrop();
    setBusy(false);
  }

  updateBackDrop() {
    UI ui = Provider.of<UI>(context);
    ui.backgroundUrl = _sections[_section][_index]?.tmdbResponse?.backDrop;
    _uiService.uiController.add(ui);
  }

  onFocusChangeUpdateSection() {
    bool sectionChanged = false;
    FocusNode currentFocusNode = WidgetsBinding.instance.focusManager.primaryFocus;
    int indexOfSection = _sections.keys.toList().indexOf(_section);

    if(currentFocusNode == bottomFocusNode) {
      // BOTTOM : next
      if(indexOfSection < _sections.length - 1 ){
        _section = _sections.keys.elementAt(indexOfSection + 1);
        _scrollController.animateTo(0, duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);
        _focusNodes[0].requestFocus();
        notifyListeners();
        sectionChanged =  true;
      } else {
        _focusNodes[_index].requestFocus();
      }
    } else if (!_focusNodes.contains(currentFocusNode)) {
      // TOP : prev
      if(indexOfSection > 0){
        _section = _sections.keys.elementAt(indexOfSection - 1);
        _scrollController.animateTo(0, duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);
        _focusNodes[0].requestFocus();
        notifyListeners();
        sectionChanged = true;
      }
    }
    if(sectionChanged) {
      for(int k = 0; k < _nbItems; k++){
        loadTranslations(k);
        loadImage(k);
      }
    }
  }

  onTapSection(String section) {
    _section = section;
    //notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
    _focusNodes.map((e) => e.dispose());
  }


}