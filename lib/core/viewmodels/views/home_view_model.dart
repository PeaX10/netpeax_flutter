
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:netpeax/core/models/item.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/core/services/trakt_service.dart';
import 'package:netpeax/core/viewmodels/base_model.dart';
import 'package:netpeax/locator.dart';
import 'package:provider/provider.dart';

class HomeViewModel extends BaseModel {
  TraktService _traktService = locator<TraktService>();

  void initUIProvider(){
    UI ui = Provider.of<UI>(context);
    ui.screenResolution = MediaQuery.of(context).size;
    ui.screenRatio = MediaQuery.of(context).devicePixelRatio;
  }

}