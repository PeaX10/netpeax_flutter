import 'package:flutter/cupertino.dart';
import 'package:netpeax/core/services/api.dart';
import 'package:meta/meta.dart';

import '../base_model.dart';

class NavbarModel extends BaseModel {
  TextEditingController searchQueryController = TextEditingController();
  String searchQuery = "";
  String active = "film";


  void startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));

    setBusy(true);
  }

  void updateSearchQuery(String newQuery) {
      searchQuery = newQuery;
      notifyListeners();
  }

  void stopSearching() {
    clearSearchQuery();

    setBusy(false);
  }

  void clearSearchQuery() {
      searchQueryController.clear();
      updateSearchQuery("");
  }

  bool isActive(String text) {
    return text == active;
  }

  void settingsPage() {

  }


}