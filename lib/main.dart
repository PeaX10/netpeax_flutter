import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netpeax/core/models/ui.dart';
import 'package:netpeax/core/services/ui_service.dart';
import 'package:netpeax/core/util/router.dart';
import 'package:netpeax/locator.dart';
import 'package:netpeax/ui/views/home_view.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(NetPeaX());
}

class NetPeaX extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<UI>(
          create: (_) => locator<UIService>().uiController.stream,
          initialData: UI.initial(),
          updateShouldNotify: (_, __) => true,
        )
      ],
      child: Shortcuts(
        shortcuts: {
          LogicalKeySet(LogicalKeyboardKey.enter): new Intent(),
        },
        // needed for AndroidTV to be able to select
        child : MaterialApp(
          title: 'NetPeaX',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent
          ),
          initialRoute: HomeView.route,
          onGenerateRoute: Router.generateRoute,
        ),
      )
    );
  }
}